let imgScroll = document.getElementById("imgScroll")
if(imgScroll !== null){
    document.addEventListener('scroll', ()=>{ 
        document.getElementById("imgScroll").style.transform = `translate(${window.pageYOffset / 100 - 25}%)`
        document.getElementById("imgScroll").style.backgroundRepeat = "repeat-x"
    })
}

let navbarScroll= document.getElementById("navbar")

    document.addEventListener('scroll', () => {
        
        if (window.pageYOffset > 50) {
            navbarScroll.classList.add('navbarCostum', 'transition')
        }else {
            navbarScroll.classList.remove('navbarCostum')
        }
        if(window.pageYOffset > 500) {
            navbarScroll.classList.add('transition')
        }else {
            navbarScroll.classList.remove('transition')
        }
    })
